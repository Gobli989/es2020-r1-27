var rows = 9;
var cols = 9;
var grid;
var w = 40;
var startTime;
var maxMines = 10;

var flagCount = maxMines;
var gameEnd = false;

const canvas = document.getElementById("game");
const ctx = canvas.getContext('2d');

// Calculates the width of the cells
function calculateWidth() {
    var f = screen.height / cols;
    w = Math.floor(f) / 2;
}

// Difficulty Selector
function startGameWithDifficulty() {

    document.getElementById("gameStatus").innerHTML = " ";
    var value = document.getElementById("difficultySelector").value;

    if (value === "easy") {
        rows = 9;
        cols = 9;
        maxMines = 10;
    } else if (value === "hard") {
        rows = 16;
        cols = 16;
        maxMines = 40;
    }

    flagCount = maxMines;

    setup();

}

// Creates a cell array
function createCellArray(rows, cols) {
    var array = new Array(cols);

    for (var i = 0; i < array.length; i++) {
        array[i] = new Array(rows);
    }
    return array;
}

// The Main function, basically setting up the whole game.
function setup() {
    // Calculating the cell width
    calculateWidth();
    // Clearing the canvas
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    canvas.width = rows * w;
    canvas.height = cols * w;

    gameEnd = false;

    grid = createCellArray(rows, cols);

    // Displays the timer and the flag counter
    document.getElementById("timer").innerHTML = "0:0";
    document.getElementById("flagCount").innerHTML = flagCount + " 🏴";

    for (var i = 0; i < cols; i++) {
        for (var j = 0; j < rows; j++) {
            grid[i][j] = new Cell(i, j, w);
        }
    }

    // Triggers when you simply click on the canvas
    canvas.addEventListener("click", function (event) {
        var x = event.clientX;
        var y = event.clientY;
        for (var i = 0; i < cols; i++) {
            for (var j = 0; j < rows; j++) {
                grid[i][j].click(x, y);
            }
        }
    });

    // Triggers when you right click on the canvas
    canvas.addEventListener('contextmenu', function (event) {

        event.preventDefault();

        var x = event.clientX;
        var y = event.clientY;

        for (var i = 0; i < cols; i++) {
            for (var j = 0; j < rows; j++) {
                grid[i][j].placeFlag(x, y);
            }
        }
    });

    // Picks random mines from the cell array
    pickRandomMines();
    // Stats the timer
    startTimer();
    // Renders the cells
    render();
}

// Picks random mines from the cell array
function pickRandomMines() {

    for (var k = 0; k < maxMines; k++) {

        var i = Math.floor(Math.random() * cols);
        var j = Math.floor(Math.random() * rows);

        var g = grid[i][j];

        if (g.mine) {
            k--;
        } else {
            g.mine = true;
        }

    }

}


// Renders the cells
function render() {
    for (var i = 0; i < cols; i++) {
        for (var j = 0; j < rows; j++) {
            grid[i][j].show();
        }
    }
}

// Stats the timer
function startTimer() {

    startTime = Date.now();

    setInterval(function () {
        if (gameEnd) return;
        var currentTime = Date.now();
        var difference = new Date(currentTime - startTime);
        document.getElementById("timer").innerHTML = difference.getMinutes() + ":" + difference.getSeconds();
    }, 1000);

}

setup();