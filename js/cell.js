var c = document.getElementById('game');
var ct = c.getContext('2d');

function Cell(i, j, w) {
	// Local Variables: Creating new cells.
	this.x = i * w;
	this.y = j * w;
	this.w = w;
	this.i = i;
	this.j = j;

	this.mine = false;
	this.revealed = false;
	this.flag = false;
}

// Show: Simple show function what makes the cell appear.
Cell.prototype.show = function () {
	// console.log("Cell.show started...");

	ct.beginPath();
	ct.fillStyle = "#e8e8e8";
	ct.strokeStyle = "#000";
	ct.rect(this.x, this.y, this.x + w, this.y + w);

	// // Cheat: Uncomment these lines to see all of the mines at the start.
	// if (this.mine) {
	// ctx.fillStyle = "#4286f4";
	// }
	ct.stroke();
	ct.fill();
}

// Click: Simple click function, what triggers when you click on a cell.
Cell.prototype.click = function (x, y) {

	// Disabled when game is already finished.
	if (gameEnd) return;

	// If The click is in the cell's region.
	if (this.x < x && this.x + this.w > x && this.y < y && this.y + this.w > y) {
		// If it's not flagged or revealed, continue
		if (!this.revealed && !this.flag) {
			// Reveal this cell.
			this.revealCell();
		}
	}
}

Cell.prototype.placeFlag = function (x, y) {

	// Disabled when game is already finished.
	if (gameEnd) return;

	// If The click is in the cell's region.
	if (this.x < x && this.x + this.w > x && this.y < y && this.y + this.w > y) {
		if (!this.revealed) {

			// If it's flagged, remove the flag and add one to the flag count
			if (this.flag) {
				this.flag = false;

				flagCount++;

				document.getElementById("flagCount").innerHTML = flagCount + " 🏴";

				// Draw an empty cell in the place of it.
				ct.beginPath();
				ct.fillStyle = "#e8e8e8";
				ct.rect(this.x + 4, this.y + 4, this.w - 5, this.w - 5);
				ct.fill();
				return;
			}

			// If it's a mine, add one to the flagged mines counter.
			if (this.mine) {
				var flaggedMines = 1;

				for (var i = 0; i < cols; i++) {
					for (var j = 0; j < rows; j++) {
						var c = grid[i][j];

						if (c.mine && c.flag) {
							flaggedMines++;
						}

						// If all the bombs are flagged, tell the player, that they won.
						if (flaggedMines === maxMines) {
							document.getElementById("gameStatus").innerHTML = "Gratulálunk, nyertél!";
							break;
						}

					}
				}
			}

			if (flagCount == 0) return;

			this.flag = true;

			// Place down a flag.
			ct.beginPath();
			ct.font = '600 20px	"Font Awesome 5 Free"';
			ct.textAlign = "center";
			ct.fillStyle = "red";
			ct.fillText("\uf024", this.x + this.w * 0.5, this.y + this.w * 0.7);

			flagCount--;

			// Decrease the flag count and tell the player that they have one flag less.

			document.getElementById("flagCount").innerHTML = flagCount + " 🏴";

		}
	}
}

// Check how many bombs are nearby
Cell.prototype.bombsNearby = function () {
	var bombs = 0;

	if (this.mine) {
		return -1;
	}

	// Calculate the nearby bomb count:
	for (var i = -1; i <= 1; i++) {
		for (var j = -1; j <= 1; j++) {
			if ((this.i + i) > -1 && (this.i + i) < cols && (this.j + j) > -1 && (this.j + j) < rows) {
				var g = grid[this.i + i][this.j + j];
				if (g.mine) {
					bombs++;
				}
			}
		}
	}
	return bombs;
}

// Reveal the cell to the player
Cell.prototype.revealCell = function () {
	this.revealed = true;

	if (this.mine) {
		// Bomb found, game over.

		gameEnd = true;

		for (var i = 0; i < cols; i++) {
			for (var j = 0; j < rows; j++) {
				var c = grid[i][j];

				if (c.mine) {
					// Draw all of the mine cells
					ct.beginPath();
					ct.fillStyle = "#ff5959";
					ct.rect(c.x, c.y, c.w, c.w);
					ct.fill();

					// Draw the Font Awesome text on it.
					ct.fillStyle = "white";
					ct.font = '600 20px "Font Awesome 5 Free"';
					ct.textAlign = "center";
					ct.fillText("\uF1E2", c.x + c.w * 0.5, c.y + c.w * 0.7);

				}
			}
		}
		// They exploded
		document.getElementById("gameStatus").innerHTML = "Legközelebb talán nagyobb szerencséd lesz!";
		return;
	} else {
		if (this.bombsNearby() == 0) {
			this.getNearbyZeros();
		}
		ct.beginPath();
		ct.fillStyle = "#bcbcbc";
		ct.rect(this.x, this.y, this.w, this.w);
		ct.fill();
		ct.fillStyle = "black";
		if (this.bombsNearby() == 0) return;
		ct.font = "20px	Palatino";
		ct.textAlign = "center";
		ct.fillText("" + this.bombsNearby(), this.x + this.w * 0.5, this.y + this.w * 0.7);


	}
}

// Nearby Cells: Shows the nearby bombs.
Cell.prototype.getNearbyZeros = function () {

	// Calculating it
	for (var i = -1; i <= 1; i++) {
		for (var j = -1; j <= 1; j++) {
			if ((this.i + i) > -1 && (this.i + i) < cols && (this.j + j) > -1 && (this.j + j) < rows) {
				var g = grid[this.i + i][this.j + j];
				if (!g.mine && !g.revealed) {
					g.revealCell();
				}
			}
		}
	}

}